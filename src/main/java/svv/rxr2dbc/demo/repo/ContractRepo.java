package svv.rxr2dbc.demo.repo;

import svv.rxr2dbc.demo.common.CommonRepository;
import svv.rxr2dbc.demo.entity.Contract;

public interface ContractRepo extends CommonRepository<Contract> {

}
