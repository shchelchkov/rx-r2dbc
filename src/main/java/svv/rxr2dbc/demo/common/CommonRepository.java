package svv.rxr2dbc.demo.common;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.repository.NoRepositoryBean;

public interface CommonRepository<E extends AbstractEntity> extends R2dbcRepository<E, Long> {
}
