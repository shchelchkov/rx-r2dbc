package svv.rxr2dbc.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
public class DemoApplicationR2DBC {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplicationR2DBC.class, args);
    }

}
