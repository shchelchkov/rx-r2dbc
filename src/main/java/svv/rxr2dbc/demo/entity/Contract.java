package svv.rxr2dbc.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import svv.rxr2dbc.demo.common.AbstractEntity;

@Data
@EqualsAndHashCode(of = "id", callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Table("ts.contract")
public class Contract extends AbstractEntity implements Comparable<Contract> {

    @Id
    private Long id;

    private Long currencyId;
    private Long exchangeId;
    private Long primaryExchangeId;
    private Long secTypeId;

    private String ticker;
    private String symbol;
    private String tradingClass;

    private Integer rank;

    @Override
    public int compareTo(Contract o) {
        return getId().compareTo(o.getId());
    }
}
