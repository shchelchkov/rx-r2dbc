package svv.rxr2dbc.demo.repo;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import reactor.core.publisher.Flux;
import svv.rxr2dbc.demo.entity.Contract;

import java.util.List;

@Slf4j
@SpringBootTest
class ContractRepoTest {

    @Autowired
    private ContractRepo contractRepo;

    @BeforeEach
    void setUp() {
    }

    @Test
    public void test_findAll() {
        List<Contract> list = contractRepo.findAll().collectList().block();
        log.info("test_findAll : {}", list.size());
    }

    @Test
    public void test_findByExample() {

        Contract employee = new Contract();
        employee.setTicker("AAPL");

        Example<Contract> example = Example.of(employee);

        Flux<Contract> employees = contractRepo.findAll(example);

        List<Contract> list = employees.collectList().block();

        log.info("test_findByExample : {}", list.size());
    }

    @AfterEach
    void tearDown() {
    }
}